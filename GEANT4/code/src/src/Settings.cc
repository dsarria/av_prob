#include "G4UnitsTable.hh"
#include "Settings.hh"
#include "G4SystemOfUnits.hh"

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
namespace GLOB
{
    // GLOBAL namespace
    // All these variables are put here to be shared amounts source files
    // (not very c++ but easier to implement)

    bool KILL_ALL_FLAG = false;

    G4String PHYSLIST;

    const double LOG10_EPSTOL = -6;

    G4String NUMBER_TO_SHOT;

    int Rand_seed;

    G4int NB_LAUNCHED = 0;

    G4String ORIENT = "+1"; // "+1" or "-1" initial particle momentum along the field or opposite direction

    int NB_1MEV_ELEC_REQ; // number of required avalanches

    double PRIMARY_ENERGY; // MeV

    const int TYPE = -1;

    std::vector<G4int> LIST_1MEV_IDS;

    double TIME_LIMIT = 0.00 * microsecond;

    double EFIELD; // V/m

    int COUNT_1MEV_ELEC = 0;

    int COUNT_AVALAN = 0;

    const G4double time_step = 0.0005 * microsecond;

    G4double drOverR = 0.0001;

    G4double SAVED_TIME = -5;

    const G4bool USE_MAX_STEP = false;

    G4double MAX_STEP = 0.1; // cm

}
