clear all
close all
clc

fn='./data/GRRR_data_1.txt';


for jj=1:1

% IMPORTING
file = importdata(fn);

E = file(:,1);
ENERGY = file(:,2); % to keV
PROB = file(:,3);

E = E*100000;

%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );
        if sum(id)>0
            
            proba=mean(PROB(id));
            prob_den{jj}(i,j)=proba;
            
        end 
    end
end

% PLOTTING
figure(jj)
h{jj}=pcolor(yy,xx,prob_den{jj}');
set(h{jj}, 'EdgeColor', 'none');
colorbar
axis([0 1000 200000 3000000])
title(['Probability to get Relativistic Avalanche; GRRR'],'interpreter','latex')
xlabel('Primary energy (keV)','interpreter','latex')
ylabel('Electric field (V/m)','interpreter','latex')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')

end


M1 = prob_den{jj}';
M2 = movmean(prob_den{jj}',[2 2]);

Vq2 = interp2(xx,yy,M1,8.e5,75)
Vq2 = interp2(xx,yy,M2,8.e5,75)

figure(2)
h{jj}=pcolor(yy,xx,M2);
set(h{jj}, 'EdgeColor', 'none');
colorbar
axis([0 1000 200000 3000000])

set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')