from string import Template
from numpy import *
import scipy.constants as co
from matplotlib import pyplot as plt

def main():
    import argparse
    
    parser = argparse.ArgumentParser()

    parser.add_argument("input",
                        help="Input file", 
                        action='store')
    args = parser.parse_args()


    data = load(args.input)
    eng = data['energy']
    field = data['field']
    prob = data['prob']
    
    plt.plot(eng, prob[100, :])
    plt.semilogy()
    plt.show()
    

                
if __name__ == '__main__':
    main()
