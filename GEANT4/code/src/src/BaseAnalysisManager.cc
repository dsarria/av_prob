//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "BaseAnalysisManager.hh"
#include <fstream>
#include <iomanip>
#include "globals.hh"
#include "G4SystemOfUnits.hh"
#include "G4Track.hh"
#include "G4ios.hh"
#include "G4SteppingManager.hh"
#include "G4ThreeVector.hh"
#include "G4PhysicalConstants.hh"


BaseAnalysisManager *BaseAnalysisManager::instance = 0;




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseAnalysisManager::BaseAnalysisManager()
{
    G4String directoryName = "./output/";

    asciiFileName2 = directoryName + std::to_string(GLOB::Rand_seed) + ".txt";

    std::ofstream asciiFile2(asciiFileName2, std::ios::trunc);
    asciiFile2.close();
}




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseAnalysisManager::~BaseAnalysisManager()
{
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseAnalysisManager *BaseAnalysisManager::getInstance()
{
    if (instance == 0) instance = new BaseAnalysisManager;

    return instance;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseAnalysisManager::analyseStepping(const G4Track)
{

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void BaseAnalysisManager::finish()
{

}


void BaseAnalysisManager::write_output_file()
{
    double proba_ava = double(GLOB::COUNT_AVALAN) / std::stod(GLOB::NUMBER_TO_SHOT) * 100.;

    G4cout << GLOB::COUNT_AVALAN << " " << std::stod(GLOB::NUMBER_TO_SHOT) << " " << proba_ava << G4endl;

    int space = 15;

    std::ofstream asciiFile2(asciiFileName2, std::ios::app);

    asciiFile2 <<  std::scientific << std::setprecision(5);

    if (asciiFile2.is_open())
        {
            asciiFile2 << std::left
                       << std::setw(space) << GLOB::Rand_seed
                       << std::setw(space) << GLOB::ORIENT
                       << std::setw(space) << GLOB::PHYSLIST
                       << std::setw(space) << GLOB::NUMBER_TO_SHOT
                       << std::setw(space) << GLOB::EFIELD
                       << std::setw(space) << GLOB::PRIMARY_ENERGY
                       << std::setw(space) << GLOB::drOverR
                       << std::setw(space) << GLOB::MAX_STEP
                       << std::setw(space) << proba_ava
                       << std::setw(space) << GLOB::NB_1MEV_ELEC_REQ << G4endl;
        }

}

