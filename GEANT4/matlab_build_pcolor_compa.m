clear all
close all
clc


[XX_GRRR,YY_GRRR,data_GRRR] = get_GRRR_pcolor();
[XX_G4,YY_G4,data_G4] = get_G4_pcolor();

data_GRRR(data_GRRR<5)=0;

data_G4_interp = interp2(XX_G4,YY_G4,data_G4',XX_GRRR,YY_GRRR);



rela_diff = abs(data_GRRR-data_G4_interp)./data_GRRR .*100;
rela_diff(isnan(rela_diff))=0;
rela_diff(isinf(rela_diff))=0;


% PLOTTING
figure
h100=pcolor(YY_GRRR,XX_GRRR,rela_diff);
set(h100, 'EdgeColor', 'none');
colorbar

title(['Relative difference (\%) between GRRR and O4 physics lists'],'interpreter','latex')
xlabel('Primary energy (keV)','interpreter','latex')
ylabel('Electric field (V/m)','interpreter','latex')


set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')


%%

function [XX,YY,data]=get_GRRR_pcolor()

fn='./data/GRRR_data_1.txt';


for jj=1:1

% IMPORTING
file = importdata(fn);

E = file(:,1);
ENERGY = file(:,2); % to keV
PROB = file(:,3);

E = E*100000;

%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );
        if sum(id)>0
            
            proba=mean(PROB(id));
            prob_den{jj}(i,j)=proba;
            
        end 
    end
end

YY = yy;
XX = xx;
data = prob_den{jj}'*100;

% PLOTTING
% figure(jj)
% h{jj}=pcolor(yy,xx,prob_den{jj}');
% set(h{jj}, 'EdgeColor', 'none');
% colorbar
% axis([0 1000 200000 3000000])
% title(['Probability to get Relativistic Avalanche; GRRR'],'interpreter','latex')
% xlabel('Primary energy (keV)','interpreter','latex')
% ylabel('Electric field (V/m)','interpreter','latex')

end

% set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.5, 0.5, 0.5]);
% set(gca, 'XScale', 'log')
% set(gca, 'YScale', 'log')
end


%% 
function [XX,YY,data]=get_G4_pcolor()


fn='./data/fused.txt';

physics_list_names{1}='O1';
physics_list_names{2}='O4';

for jj=2:2

LIST_selected=physics_list_names{jj};
DIRECTION_selected='+1';

% IMPORTING
file = importdata(fn);
DIRECTION = file.textdata(:,2);
LIST = file.textdata(:,3); %('O1' or 'O4')


SEED = file.textdata(:,1);
E = file.data(:,2);
ENERGY = file.data(:,3)*1000; % to keV
N_AV = file.data(:,1);
PROB = file.data(:,4);



%% filtering physics list
SEED=SEED(strcmp(LIST,LIST_selected));
E=E(strcmp(LIST,LIST_selected));
ENERGY=ENERGY(strcmp(LIST,LIST_selected));
N_AV=N_AV(strcmp(LIST,LIST_selected));
PROB=PROB(strcmp(LIST,LIST_selected));
DIRECTION=DIRECTION(strcmp(LIST,LIST_selected));

%% filtering direction
SEED=SEED(strcmp(DIRECTION,DIRECTION_selected));
E=E(strcmp(DIRECTION,DIRECTION_selected));
ENERGY=ENERGY(strcmp(DIRECTION,DIRECTION_selected));
N_AV=N_AV(strcmp(DIRECTION,DIRECTION_selected));
PROB=PROB(strcmp(DIRECTION,DIRECTION_selected));



%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );
        
        if sum(id)>0
            
            stats_ini=N_AV(id);
            proba=sum(PROB(id).*stats_ini)/sum(stats_ini);
            
            similar_probas_lists{i,j}=PROB(id);
            
            prob_den{jj}(i,j)=proba;
            
        end
        
    end
end

XX=xx;
YY=yy;
data=prob_den{2};

% PLOTTING
% figure(jj)
% h{jj}=pcolor(yy,xx,prob_den{jj}');
% set(h{jj}, 'EdgeColor', 'none');
% colorbar
% axis([0 1000 200000 3000000])
% title(['Probability to get Relativistic Avalanche; ' LIST_selected],'interpreter','latex')
% xlabel('Primary energy (keV)','interpreter','latex')
% ylabel('Electric field (V/m)','interpreter','latex')

end


end