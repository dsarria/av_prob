//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// Code developed by:
//  S.Larsson
//
//    ***********************************
//    *                                 *
//    *    BaseTrackingAction.cc     *
//    *                                 *
//    ***********************************
//
// $Id: BaseTrackingAction.cc 72967 2013-08-14 14:57:48Z gcosmo $
//

#include "BaseTrackingAction.hh"
#include "BaseRunAction.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"


#include "G4TrackingManager.hh"
#include "G4Track.hh"
#include "G4TrackVector.hh"

#include "BaseAnalysisManager.hh"
#include "BaseDetectorConstruction.hh"

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>



extern int NB_1MEV_ELEC_REQ;
extern bool KILL_ALL_FLAG;
extern int COUNT_1MEV_ELEC;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseTrackingAction::BaseTrackingAction()
{}

BaseTrackingAction::~BaseTrackingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
void BaseTrackingAction::PreUserTrackingAction(const G4Track *)
{

}

void BaseTrackingAction::PostUserTrackingAction(const G4Track *)
{

}
