#!/usr/bin/env python


from subprocess import call
from functools import partial
from multiprocessing.dummy import Pool
import numpy as np
import random
import time
import sys
from datetime import datetime


random.seed(datetime.now())
rand_seed = random.randint(1,100000000)

# define the commands to be run in parallel
    
nr_particles = 1000
nr_1MEV_elec_required = 20
#list_primary_energy = np.logspace(np.log10(0.010),np.log10(1.),50)  # MeV
list_primary_energy = [0.6,0.3,0.1,0.08]
#list_physicsList= ['O1','O4','SS']
#list_physicsList= ['O4','O1']
list_physicsList= ['O1']
#list_physics= ['LIV','O4','O1','PEN']
list_Efields = np.linspace(2e5,15e5,10) # V/m
list_direction=["+1"]
#max_step_list = [0.00001,0.00003,0.0001,0.0003,0.001,0.003,0.01,0.03,0.1,0.3,1,3,10,30.,100., 300.,1000.] # mm
#max_step_list = [0.001,0.003,0.01,0.03,0.1,0.3,1,3,10,30.,100., 300.,1000.] # mm
#dROverR_list = [0.8] # dimensionless
#dROverR_list = [0.00005, 0.00001,0.000005, 0.000001] # dimensionless
dROverR_list = [0.8]
max_step_list = [10.0,5.0,1.0,0.1,0.01] # cm

nb_runs = 1000

commands=[]

excecutable = './BaseAV'

for direction in list_direction:
    for primary_energy in list_primary_energy:
        for field in list_Efields:
            for dROverR in dROverR_list:
                for physicsList in list_physicsList:
                    for _ in range(nb_runs):
                        for max_step in max_step_list:
                          commands.append(excecutable+' '+str(physicsList)+' '+str(nr_particles)+' '+str(field)
                                          +' '+str(primary_energy)+' '+str(nr_1MEV_elec_required)+' '+str(rand_seed)+' '+str(direction)+' '+str(dROverR)+' '+str(max_step))
                          rand_seed=rand_seed+1


command_number=len(commands)

print('Number of commands required '+ str(command_number))

pool = Pool(7) # Nomber of threads
for i, returncode in enumerate(pool.imap(partial(call, shell=True), commands)):
    if returncode != 0:
       print("%d command failed: %d" % (i, returncode))
