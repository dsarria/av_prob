//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "BaseEventAction.hh"

#include "BaseRunAction.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"

#include"BaseAnalysisManager.hh"
#include "G4SystemOfUnits.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseEventAction::BaseEventAction()
    : drawFlag("all"), printModulo(10000)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseEventAction::~BaseEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseEventAction::BeginOfEventAction(const G4Event *)
{

    GLOB::NB_LAUNCHED++;

    GLOB::KILL_ALL_FLAG = false;

    GLOB::COUNT_1MEV_ELEC = 0;

    GLOB::LIST_1MEV_IDS.clear();

    GLOB::TIME_LIMIT = GLOB::time_step;

    GLOB::SAVED_TIME = -5.0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseEventAction::EndOfEventAction(const G4Event *)
{

    G4String has_hit_NB_1MEV_elecs = "false";

    if (GLOB::COUNT_1MEV_ELEC >= GLOB::NB_1MEV_ELEC_REQ)
        {
            GLOB::COUNT_AVALAN++;
            has_hit_NB_1MEV_elecs = "true";
//            G4cout << GLOB::SAVED_TIME / nanosecond << G4endl;
        }
    else
        {
//                        G4cout << GLOB::SAVED_TIME / nanosecond << G4endl;
        }

    //    G4cout << GLOB::SAVED_TIME / millisecond << " " << has_hit_NB_1MEV_elecs << G4endl;


    //     G4cout << GLOB::COUNT_AVALAN << " " << GLOB::NB_LAUNCHED << " " << double(GLOB::COUNT_AVALAN) / double(GLOB::NB_LAUNCHED) * 100. << G4endl;

}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

















