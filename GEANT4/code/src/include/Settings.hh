#pragma once

#include <vector>
// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
namespace GLOB
{
    // GLOBAL namespace
    // All these variables are put here to be shared amounts source files
    // (not very c++ but easier to implement)

    extern bool KILL_ALL_FLAG;

    extern G4String PHYSLIST;

    extern const double LOG10_EPSTOL;

    extern G4String NUMBER_TO_SHOT;

    extern int Rand_seed;

    extern G4int NB_LAUNCHED ;

    extern G4String ORIENT; // "+1" or "-1" initial particle momentum along the field or opposite direction

    extern int NB_1MEV_ELEC_REQ; // number of required avalanches

    extern double PRIMARY_ENERGY; // MeV

    extern const int TYPE;

    extern std::vector<G4int> LIST_1MEV_IDS;

    extern double TIME_LIMIT;

    extern double EFIELD; // V/m

    extern int COUNT_1MEV_ELEC;

    extern int COUNT_AVALAN;

    extern const G4double time_step;

    extern G4double drOverR;

    extern G4double SAVED_TIME;

    extern const G4bool USE_MAX_STEP;

    extern G4double MAX_STEP;

}
