clear all
close all
clc

files={'./data/fused.txt','./data/GRRR_data_1.txt','./data/charion_fig8_05Ek.txt'};

physics_list_names{1}='O1';
physics_list_names{2}='O4';

%vector for lines in contour plot
v = [1.6e+6];




fn = files{1};

for jj=1:2

LIST_selected=physics_list_names{jj};
DIRECTION_selected='+1';

% IMPORTING
file = importdata(fn);
DIRECTION = file.textdata(:,2);
LIST = file.textdata(:,3); %('O1' or 'O4')


SEED = file.textdata(:,1);
E = file.data(:,2);
ENERGY = file.data(:,3)*1000; % to keV
N_AV = file.data(:,1);
PROB = file.data(:,4);



%% filtering physics list
SEED=SEED(strcmp(LIST,LIST_selected));
E=E(strcmp(LIST,LIST_selected));
ENERGY=ENERGY(strcmp(LIST,LIST_selected));
N_AV=N_AV(strcmp(LIST,LIST_selected));
PROB=PROB(strcmp(LIST,LIST_selected));
DIRECTION=DIRECTION(strcmp(LIST,LIST_selected));

%% filtering direction
SEED=SEED(strcmp(DIRECTION,DIRECTION_selected));
E=E(strcmp(DIRECTION,DIRECTION_selected));
ENERGY=ENERGY(strcmp(DIRECTION,DIRECTION_selected));
N_AV=N_AV(strcmp(DIRECTION,DIRECTION_selected));
PROB=PROB(strcmp(DIRECTION,DIRECTION_selected));



%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );

        if sum(id)>0

            stats_ini=N_AV(id);
            proba=sum(PROB(id).*stats_ini)/sum(stats_ini);

            similar_probas_lists{i,j}=PROB(id);

            prob_den{jj}(i,j)=proba;

        end

    end
end


end


figr = importdata(files{2});

Egr = figr(:,1);
ENERGY_gr = figr(:,2);
PROB_gr = figr(:,3);


Efieldgr = unique(Egr);
Ep_gr = unique(ENERGY_gr);
ENERGY_grid_gr=sort(Ep_gr);
%Efield_grid_gr = Efieldgr;
Efield_grid_gr=sort(Efieldgr);
% adding 0 field (for cosmetics in the plot)
%Efield_grid_gr=[0;Efield_grid_gr];
n1gr = length(Efield_grid_gr);
n2gr = length(ENERGY_grid_gr);


Egr=reshape(Egr,[n2gr,n1gr]);
ENERGY_gr = reshape(ENERGY_gr,[n2gr,n1gr]);
PROB_gr = reshape(PROB_gr,[n2gr,n1gr]);

%kV/cm to V/m
Egr = Egr*10^5;
PROB_gr = PROB_gr*100.0;
%Ep_gr = Ep_gr*10^5;

figc = importdata(files{3});



ENERGY_ch = figc(:,1);
PROB_ch = figc(:,2);


%Chanrion data to keV and percentage

ENERGY_ch = ENERGY_ch.*10^-3;
PROB_ch = PROB_ch.*100.0;



%-------TOO SLOW----------
%figure;

%geO1 = contour(yy,prob_den{1}',xx,v);
%hold on;
%geO4 = countour(yy,prob_den{2}',xx,v);

%------------------------

%---- find indices of 0.5Ek on GEANT4 and GRRR------


[ige,jge] = find((xx > 1.6e+6)&(xx < 1.63e+6));

[igr,jgr] = find((Egr > 1.6e+6)&(Egr < 1.63e+6));

prob_gr = PROB_gr(:,jgr(1));
prob_geO1 = prob_den{1}(jge(1),:);
prob_geO4 = prob_den{2}(jge(1),:);




figure;

gr = semilogx(Ep_gr,prob_gr,'r','LineWidth',2);
hold on
geO1 = semilogx(Ep,prob_geO1,'m','LineWidth',2);
geO4 = semilogx(Ep,prob_geO4,'k','LineWidth',2);
ch16mc = semilogx(ENERGY_ch,PROB_ch,'g','LineWidth',2);
ch16fp = line([102,102],[0,100],'LineWidth',2);
grid on
xlabel('Energy [keV]')
ylabel('Probability [%]')
title('Electric field = 0.5E_{k}')
legend('GRRR','GEANT4_O1','GEANT4_O4','Chanrion et al. (2016) MC','Chanrion et al. (2016) FP','Location','northwest')
print('compare_ch','-dpng')
