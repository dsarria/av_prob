from string import Template
from numpy import *
import scipy.constants as co

def main():
    with open("avalanche_template.yaml") as fin:
        tmpl = Template(fin.read())

    eng = logspace(1, 3, 50)
    field = logspace(0, log10(30), 200)

    for i, ifield in enumerate(field):
        for j, jeng in enumerate(eng):
            fname = 'in/avalanche_%.4d_%.4d.yaml' % (i, j)
            
            with open(fname, "w") as fij:
                fij.write(tmpl.safe_substitute(
                    i="%.4d" % i,
                    j="%.4d" % j,
                    init_particle_energy=str(jeng * co.kilo * co.eV),
                    EB=ifield * co.kilo / co.centi))


                
if __name__ == '__main__':
    main()
