#include "BasePhysicsList.hh"
#include "G4EmStandardPhysics_option4.hh"
#include "G4EmStandardPhysics_option3.hh"
#include "G4EmStandardPhysics_option2.hh"
#include "G4EmStandardPhysics_option1.hh"

#include "G4EmStandardPhysics_option1_dr.hh"
#include "G4EmStandardPhysics_option4_dr.hh"

#include "G4EmLivermorePhysics.hh"
#include "G4EmLivermorePolarizedPhysics.hh"
#include "G4EmPenelopePhysics.hh"
#include "G4ProductionCutsTable.hh"
#include "G4EmLowEPPhysics.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4EmStandardPhysicsGS.hh"
#include "G4EmStandardPhysicsSS.hh"

#include "G4EmStandardPhysicsWVI.hh"

#include "G4RadioactiveDecay.hh"
#include "G4NeutrinoE.hh"
#include "G4AntiNeutrinoE.hh"
#include "G4Alpha.hh"
#include "G4GenericIon.hh"
#include "G4PhysicsListHelper.hh"
#include "G4UnitsTable.hh"
#include "G4EmParameters.hh"

#include "G4EmCalculator.hh"
#include "G4Electron.hh"

#include "BaseDetectorConstruction.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BasePhysicsList::BasePhysicsList(): G4VUserPhysicsList()
{
    defaultCutValue = 1.*micrometer;
    cutForElectron = defaultCutValue;
    cutForPositron = defaultCutValue;


    cutForGamma =  1.*micrometer;


    G4cout << GLOB::PHYSLIST << G4endl;

    G4String PHYSLIST = GLOB::PHYSLIST;

    if (PHYSLIST == "O4")
        {
            //            emPhysicsList = new G4EmStandardPhysics_option4();
            emPhysicsList = new G4EmStandardPhysics_option4_dr();
        }
    else if (PHYSLIST == "LIV")
        {
            emPhysicsList = new G4EmLivermorePhysics();
        }
    else if (PHYSLIST == "PEN")
        {
            emPhysicsList = new G4EmPenelopePhysics();
        }
    else if (PHYSLIST == "LIVPOLAR")
        {
            emPhysicsList = new G4EmLivermorePolarizedPhysics();
        }
    else if (PHYSLIST == "O3")
        {
            emPhysicsList = new G4EmStandardPhysics_option3();
        }
    else if (PHYSLIST == "O2")
        {
            emPhysicsList = new G4EmStandardPhysics_option2();
        }
    else if (PHYSLIST == "O1")
        {
            //            emPhysicsList = new G4EmStandardPhysics_option1();
            emPhysicsList = new G4EmStandardPhysics_option1_dr();
        }
    else if (PHYSLIST == "LEP")
        {
            emPhysicsList = new G4EmLowEPPhysics();
        }
    else if (PHYSLIST == "GS")
        {
            emPhysicsList = new G4EmStandardPhysicsGS();
        }
    else if (PHYSLIST == "SS")
        {
            emPhysicsList = new G4EmStandardPhysicsSS();
        }
    else if (PHYSLIST == "WVI")
        {
            emPhysicsList = new G4EmStandardPhysicsWVI();
        }
    else
        {
            G4cout << "WRONG PHYSICS LIST NAME" << G4endl;
            std::abort();
        }


    this->DumpCutValuesTable();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BasePhysicsList::~BasePhysicsList()
{
    delete emPhysicsList;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BasePhysicsList::ConstructParticle()
{
    emPhysicsList->ConstructParticle();


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BasePhysicsList::ConstructProcess()
{
    AddTransportation();
    emPhysicsList->ConstructProcess();

    if (GLOB::USE_MAX_STEP)
        {
            AddStepMax(GLOB::MAX_STEP * cm, 100.*m); // different max step for photons and electrons/positrons
        }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BasePhysicsList::SetCuts()
{
    SetCutValue(cutForGamma, "gamma");
    SetCutValue(cutForElectron, "e-");
    SetCutValue(cutForPositron, "e+");

    //    G4EmParameters *param = G4EmParameters::Instance();
    //    param->SetMscStepLimitType(fUseSafetyPlus);

    //     param->SetUseMottCorrection(true);
    //    param->SetDefaults();
    //    param->SetLowestElectronEnergy(10 * eV);
    //  param->SetLatDisplacementBeyondSafety(true);
    //    param->SetMuHadLateralDisplacement(false);
    //    param->ActivateAngularGeneratorForIonisation(true);
    //    param->SetMscThetaLimit(0.0);
    //    param->SetFluo(true);



    //    param->SetDefaults();
    //    param->SetLowestElectronEnergy(10 * eV);
    //    param->SetMscThetaLimit(0.0);
    //    param->SetFluo(true);
    //    param->SetAuger(true);
    //    param->SetPixe(true);
    //    G4double lowlimit=ENERGY_THRES*CLHEP::keV;
    //    G4ProductionCutsTable * aPCTable = G4ProductionCutsTable::GetProductionCutsTable();
    //    aPCTable->SetEnergyRange(lowlimit,100*CLHEP::GeV);

    //    param->SetMinEnergy(ENERGY_THRES*CLHEP::keV);
    //   param->SetMaxEnergy(100.*MeV);
    //   param->SetLowestElectronEnergy(ENERGY_THRES*CLHEP::eV);
    //       param->SetNumberOfBinsPerDecade(100);
    //    param->SetMscRangeFactor(0.005);
    //   param->SetFluo(true);
    //   param->SetAuger(true);
    //   param->SetPixe(true);
    //   param->SetPIXEElectronCrossSectionModel("Penelope");
    //       param->SetLateralDisplacement(true);
    //       param->ActivateAngularGeneratorForIonisation(true);

}



// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "G4ProcessManager.hh"
#include "StepMax.hh"

void BasePhysicsList::AddStepMax(G4double stepMaxVal_elec, G4double stepMaxVal_gamma)
{
    // Step limitation seen as a process
    StepMax *stepMaxProcess_elec = new StepMax();
    stepMaxProcess_elec->SetMaxStep(stepMaxVal_elec);

    StepMax *stepMaxProcess_gamma = new StepMax();
    stepMaxProcess_gamma->SetMaxStep(stepMaxVal_gamma);

    auto particleIterator = GetParticleIterator();
    particleIterator->reset();

    while ((*particleIterator)())
        {
            G4ParticleDefinition *particle = particleIterator->value();
            G4ProcessManager *pmanager = particle->GetProcessManager();

            if (stepMaxProcess_elec->IsApplicable(*particle))
                {
                    if (particle->GetPDGEncoding() == 22)
                        {
                            pmanager ->AddDiscreteProcess(stepMaxProcess_gamma);
                        }
                    else
                        {
                            pmanager ->AddDiscreteProcess(stepMaxProcess_elec);
                        }
                }
        }
}
