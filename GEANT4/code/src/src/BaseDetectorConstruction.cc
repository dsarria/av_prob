//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "BaseDetectorConstruction.hh"
#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ThreeVector.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4PVParameterised.hh"
#include "G4Mag_UsualEqRhs.hh"
#include "G4FieldManager.hh"
#include "G4UniformElectricField.hh"
#include "G4TransportationManager.hh"
#include "G4EqMagElectricField.hh"
#include "G4ElectroMagneticField.hh"

#include "G4TransportationManager.hh"
#include "G4PropagatorInField.hh"

#include "G4ChordFinder.hh"
#include "G4UniformMagField.hh"
#include "G4ExplicitEuler.hh"
#include "G4ImplicitEuler.hh"
#include "G4SimpleRunge.hh"
#include "G4SimpleHeum.hh"
#include "G4ClassicalRK4.hh"
#include "G4HelixExplicitEuler.hh"
#include "G4HelixImplicitEuler.hh"
#include "G4HelixSimpleRunge.hh"
#include "G4CashKarpRKF45.hh"
#include "G4RKG3_Stepper.hh"

#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4UnitsTable.hh"
#include "G4ios.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Possibility to turn off (0) magnetic field and measurement volume.
//#define Layer 1          // Magnet geometric volume
#define MAG 1          // Magnetic field grid
#define MEASUREVOL 1   // Volume for measurement
//#define DETECTORVOL 1  // Volume for detectors

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


BaseDetectorConstruction *BaseDetectorConstruction::instance = 0;

BaseDetectorConstruction::BaseDetectorConstruction()
    : physiWorld(NULL), logicWorld(NULL), solidWorld(NULL),
      physiMeasureVolume(NULL), logicMeasureVolume(NULL), solidMeasureBox(NULL),
      physDetector1(NULL), logicDetector1(NULL), detectorBox1(NULL),
      physDetector2(NULL), logicDetector2(NULL), detectorBox2(NULL),
      WorldMaterial(NULL),
      MeasureMaterial(NULL)

{
    WorldSizeXY = WorldSizeZ = 0;
    MeasureVolumeSizeXY = MeasureVolumeSizeZ = 0;
}

BaseDetectorConstruction *BaseDetectorConstruction::getInstance()
{
    if (instance == 0) instance = new BaseDetectorConstruction;

    return instance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseDetectorConstruction::~BaseDetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4VPhysicalVolume *BaseDetectorConstruction::Construct()

{
    DefineMaterials();
    return ConstructCalorimeter();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseDetectorConstruction::DefineMaterials()
{
    G4String name, symbol;
    G4double density;

    G4int ncomponents;
    G4double fractionmass;

    // Define Elements

    G4Element   *elN  = new G4Element("Nitrogen", "N",  7., 14.01 * g / mole);
    G4Element   *elO  = new G4Element("Oxygen"  , "O",  8., 16.00 * g / mole);
    G4Element   *elAr = new G4Element("Argon" , "Ar",  18., 39.948 * g / mole);

    density = 1.293   * kg / m3;
    MeasureMaterial = new G4Material(name = "air_" + std::to_string(0), density, ncomponents = 3);
    MeasureMaterial->AddElement(elN, fractionmass = 0.755);
    MeasureMaterial->AddElement(elO, fractionmass = 0.2322);
    MeasureMaterial->AddElement(elAr, fractionmass = 0.0128);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

G4VPhysicalVolume *BaseDetectorConstruction::ConstructCalorimeter()
{

    G4double MinStep     = 1.*micrometer ; // minimal step, micrometer

    G4double minEps = pow(10., -5); //   Minimum & value for smallest steps
    G4double maxEps = pow(10., -5); // ! : useless if G4ExactUniEFStepper is specified

    // Create an equation of motion for glubal field (zero)
    G4ElectricField *myEfield = new G4UniformElectricField(G4ThreeVector(0.0, 0.0, -GLOB::EFIELD * volt / meter));

    G4EqMagElectricField *Equation = new G4EqMagElectricField(myEfield);

    G4int nvar = 8; // to integrate time and energy

    G4MagIntegratorStepper *EStepper = new G4ClassicalRK4(Equation , nvar);
    //     G4MagIntegratorStepper* EStepper = new G4ExactUniEFStepper( Equation);

    // Get the global field manager
    G4FieldManager *globalfieldMgr = G4TransportationManager::GetTransportationManager()->GetFieldManager();
    // Set this field to the global field manager
    globalfieldMgr->SetDetectorField(myEfield);

    G4MagInt_Driver *EIntgrDriver = new G4MagInt_Driver(MinStep,
            EStepper,
            EStepper->GetNumberOfVariables());

    G4ChordFinder *EChordFinder = new G4ChordFinder(EIntgrDriver);
    globalfieldMgr->SetChordFinder(EChordFinder);

    globalfieldMgr->SetMinimumEpsilonStep(minEps);   // ! : useless if G4ExactUniEFStepper is specified
    globalfieldMgr->SetMaximumEpsilonStep(maxEps);

    // Create an equation of motion for local field
    G4ElectricField *myLocalEfield = new G4UniformElectricField(G4ThreeVector(0.0, 0.0, -GLOB::EFIELD * volt / meter));
    G4EqMagElectricField *EquationLocal = new G4EqMagElectricField(myLocalEfield);

    G4MagIntegratorStepper *EStepper2 = new G4ClassicalRK4(EquationLocal, nvar);
    //     G4MagIntegratorStepper* EStepper2 = new G4ExactUniEFStepper( EquationLocal );

    G4FieldManager *localfieldMgr = new G4FieldManager();
    localfieldMgr->SetDetectorField(myLocalEfield);
    G4MagInt_Driver *EIntgrDriver2 = new G4MagInt_Driver(MinStep,
            EStepper2,
            EStepper2->GetNumberOfVariables());
    G4ChordFinder *EChordFinder2 = new G4ChordFinder(EIntgrDriver2);
    localfieldMgr->SetChordFinder(EChordFinder2);

    localfieldMgr->SetMinimumEpsilonStep(minEps);   // ! : useless if G4ExactUniEFStepper is specified
    localfieldMgr->SetMaximumEpsilonStep(maxEps);

    globalfieldMgr->SetDeltaOneStep(1.e-3 * mm);
    localfieldMgr->SetDeltaOneStep(1.e-3 * mm);

    //////////////////////////////////////
    // NOT USED ANYMORE. A STEP MAX IN THE PHYSICS LIST IS PREFERED
    //    G4TransportationManager::GetTransportationManager()->GetPropagatorInField()->SetLargestAcceptableStep(0.002*mm);

    // Complete the parameters definition

    //The World
    WorldSizeZ   =  4000.*m / 2.;
    WorldSizeXY  =  4000.*m / 2.; // Cube

    //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
    //
    // World
    //

    solidWorld = new G4Box("World",				    //its name
                           WorldSizeXY, WorldSizeXY, WorldSizeZ); //its size


    logicWorld = new G4LogicalVolume(solidWorld,	    //its solid
                                     MeasureMaterial,   //its material
                                     "World",         //its name
                                     0, 0, 0);

    physiWorld = new G4PVPlacement(0,                 //no rotation
                                   G4ThreeVector(0., 0., 0.),	//at (0,0,0)
                                   "World",           //its name
                                   logicWorld,		//its logical volume
                                   NULL,              //its mother  volume
                                   false,             //no boolean operation
                                   0);                //copy number


    G4Box *atmosLayer_S = new G4Box("solidBox", WorldSizeXY, WorldSizeXY, WorldSizeZ);

    G4LogicalVolume *atmosLayer_LV = new G4LogicalVolume(atmosLayer_S, MeasureMaterial, "logicBox", localfieldMgr);

    G4VPhysicalVolume *atmosLayer_PV = new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), "physBox",
            atmosLayer_LV, physiWorld, false, 0, 1);

    return physiWorld;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


