//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "Randomize.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif


//#include "G4EmPenelopePhysics.hh"

#include <G4PhysListFactory.hh>

#include "BaseDetectorConstruction.hh"
#include "BasePrimaryGeneratorAction.hh"
#include "BaseRunAction.hh"
#include "BaseEventAction.hh"
#include "BaseTrackingAction.hh"

#include "BaseStackingAction.hh"

#include "BaseSteppingAction.hh"

#include "BasePhysicsList.hh"
#include "G4NistManager.hh"
#include <time.h>
#include <iostream>
#include <fstream>

#include <time.h>
#include <sys/time.h>

#include "Settings.hh"


double get_wall_time()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + (tv.tv_usec / 1000000.0);
}

int main(int argc, char **argv)
{
    G4cout << argc << G4endl;

    if (argc >= 3)
        {

            //   std::cout << "Argument list" << std::endl;
            //     for (int i = 0; i < argc; i++)
            //         { std::cout << argv[i] << std::endl;}

            GLOB::PHYSLIST = argv[1];
            GLOB::NUMBER_TO_SHOT = argv[2];
            GLOB::EFIELD = std::stod(argv[3]); // V/m
            GLOB::PRIMARY_ENERGY = std::stod(argv[4]);
            GLOB::NB_1MEV_ELEC_REQ = std::stoi(argv[5]);
            GLOB::Rand_seed = std::stoi(argv[6]);
            GLOB::ORIENT = argv[7];
            GLOB::drOverR = std::stod(argv[8]);
            GLOB::MAX_STEP = std::stod(argv[9]);
        }
    else
        {
            GLOB::PHYSLIST = "O1";
            GLOB::NUMBER_TO_SHOT = "1000";
            GLOB::EFIELD = 14.e5; // V/m
            GLOB::PRIMARY_ENERGY = 0.06; // MeV
            GLOB::NB_1MEV_ELEC_REQ = 20;
            GLOB::Rand_seed = 797;
            GLOB::ORIENT = "+1";
            GLOB::drOverR = 0.1;
            GLOB::MAX_STEP = 0.1;
        }

    //     G4NistManager* man = G4NistManager::Instance();
    //   man->SetVerbose(1);
    //   G4Material* H2O = man->FindOrBuildMaterial("G4_WATER");

    //choose the Random engine
    G4int seed = GLOB::Rand_seed;
    //  G4Random::setTheEngine(new CLHEP::MTwistEngine);
    G4Random::setTheSeed(seed);

    //Base Verbose output class
    //   G4VSteppingVerbose::SetInstance(new BaseSteppingVerbose);

    // Construct the default run manager
    G4RunManager *runManager = new G4RunManager;

    // set mandatory initialization classes
    BaseDetectorConstruction *detector = new BaseDetectorConstruction();
    runManager->SetUserInitialization(detector);

    runManager->SetUserInitialization(new BasePhysicsList);

    //    G4PhysListFactory* physListFactory= new G4PhysListFactory();
    //    G4VUserPhysicsList* physicsList=physListFactory->GetReferencePhysList("LBE");
    //    runManager->SetUserInitialization(physicsList);

    runManager->SetUserAction(new BasePrimaryGeneratorAction());

#ifdef G4VIS_USE
    // visualization manager
    G4VisManager *visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    // set user action classes
    BaseRunAction *RunAct = new BaseRunAction();
    runManager->SetUserAction(RunAct);
    runManager->SetUserAction(new BaseEventAction());

    // G4cout << filename1 << G4endl;

    //    BaseTrackingAction* trackAction = new BaseTrackingAction();
    //    runManager->SetUserAction(trackAction);

    G4UserStackingAction *stackingAction = new BaseStackingAction();
    runManager->SetUserAction(stackingAction);


    //SteppingAction
    runManager->SetUserAction(new BaseSteppingAction(detector));


    //Initialize G4 kernel
    runManager->Initialize();

    // get the pointer to the User Interface manager
    G4UImanager *UImanager = G4UImanager::GetUIpointer();


    UImanager->ApplyCommand("/run/beamOn " + GLOB::NUMBER_TO_SHOT);
    //    UImanager->ApplyCommand("/process/eLoss/integral true");

    // job termination
#ifdef G4VIS_USE
    delete visManager;
#endif
    delete runManager;

    return 0;
}

