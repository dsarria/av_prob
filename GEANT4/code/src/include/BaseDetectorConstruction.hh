//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#ifndef BaseDetectorConstruction_h
#define BaseDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "Settings.hh"

class G4Box;
class G4Trd;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4UniformMagField;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

class BaseDetectorConstruction : public G4VUserDetectorConstruction
{
    public:
        BaseDetectorConstruction();
        ~BaseDetectorConstruction();

    public:

        G4VPhysicalVolume *Construct();

    public:

        static BaseDetectorConstruction *getInstance();

        void PrintDetectorParameters();

        G4double GetWorldSizeXY()
        {
            return WorldSizeXY;
        };
        G4double GetWorldSizeZ()
        {
            return WorldSizeZ;
        };

        G4double GetMeasureVolumeSizeXY()
        {
            return MeasureVolumeSizeXY;
        };
        G4double GetMeasureVolumeSizeZ()
        {
            return MeasureVolumeSizeZ;
        };

        G4Material *GetWorldMaterial()
        {
            return WorldMaterial;
        };
        G4Material *GetMeasureMaterial()
        {
            return MeasureMaterial;
        };

        const G4VPhysicalVolume *GetWorld()
        {
            return physiWorld;
        };
        const G4VPhysicalVolume *GetMeasureVolume()
        {
            return physiMeasureVolume;
        };
        const G4VPhysicalVolume *GetDetector1()
        {
            return physDetector1;
        };
        const G4VPhysicalVolume *GetDetector2()
        {
            return physDetector2;
        };


    private:

        G4double           WorldSizeXY;
        G4double           WorldSizeZ;

        G4double           MeasureVolumeSizeXY;
        G4double           MeasureVolumeSizeZ;
        G4double           MeasureVolumePosition;

        G4VPhysicalVolume *physBox1;
        G4LogicalVolume   *logicBox1;
        G4Box             *solidBox1;

        G4VPhysicalVolume *physiWorld;
        G4LogicalVolume   *logicWorld;
        G4Box             *solidWorld;

        G4VPhysicalVolume *physiMeasureVolume;
        G4LogicalVolume   *logicMeasureVolume;
        G4Box             *solidMeasureBox;

        G4VPhysicalVolume *physDetector1;
        G4LogicalVolume   *logicDetector1;
        G4Box             *detectorBox1;

        G4VPhysicalVolume *physDetector2;
        G4LogicalVolume   *logicDetector2;
        G4Box             *detectorBox2;

        G4Material        *WorldMaterial;
        G4Material        *MeasureMaterial;

    private:
        static BaseDetectorConstruction *instance;
        void DefineMaterials();
        G4VPhysicalVolume *ConstructCalorimeter();
};


#endif


