//
// ********************************************************************

//

#include "BaseStackingAction.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4Track.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleTypes.hh"

#include "G4SystemOfUnits.hh"
#include "G4ios.hh"


BaseStackingAction::BaseStackingAction()
{
}

BaseStackingAction::~BaseStackingAction()
{  }


G4ClassificationOfNewTrack
BaseStackingAction::ClassifyNewTrack(const G4Track *aTrack)
{
    G4ClassificationOfNewTrack classification;

    G4int ID = aTrack->GetTrackID();
    G4double ener = aTrack->GetKineticEnergy();

    G4int PDG_enc = aTrack->GetParticleDefinition()->GetPDGEncoding();

    GLOB::SAVED_TIME = aTrack->GetGlobalTime();

    if (PDG_enc != 11)
        {
            classification = fUrgent;
            return classification;
        }

    //    ini_elec_ener = ener;
    //    elec_time = aTrack->GetGlobalTime();
    //    part_name = aTrack->GetParticleDefinition()->GetParticleName();

    if (GLOB::KILL_ALL_FLAG)
        {
            classification = fKill;
            return classification;
        }

    if (ener < 4.*keV)
        {
            classification = fKill;
            return classification;
        }


    if (aTrack->GetTrackStatus() == fSuspend)
        {
            classification = fWaiting;
            return classification;
        }

    // if energy > 1 MeV and if the ID is not already saved
    if ((ener > 1.*MeV) && ID_not_already_saved(ID, GLOB::LIST_1MEV_IDS))
        {
            GLOB::LIST_1MEV_IDS.push_back(ID); // save the ID
            GLOB::COUNT_1MEV_ELEC++; // increment counter

            if (GLOB::COUNT_1MEV_ELEC >= GLOB::NB_1MEV_ELEC_REQ)
                {
                    GLOB::KILL_ALL_FLAG = true;
                    GLOB::SAVED_TIME = aTrack->GetGlobalTime();
                }
        }

    // default classification
    classification = fUrgent;
    return classification;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseStackingAction::print_status()
{
    G4cout << "current max time : " << GLOB::TIME_LIMIT / microsecond << " microsecond" << G4endl;
    G4cout << ">1MeV electron count : " << GLOB::COUNT_1MEV_ELEC << G4endl;
    G4cout << "electron energy : " << ini_elec_ener / keV << G4endl;
    G4cout << "electron time : " << elec_time / microsecond << G4endl;
    G4cout << part_name << G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseStackingAction::NewStage()
{
    GLOB::TIME_LIMIT += GLOB::time_step;
    //    print_status();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

bool BaseStackingAction::ID_not_already_saved(const G4int ID, const std::vector<G4int> LIST_1MEV_IDS_ar)
{
    for (auto ID_read : LIST_1MEV_IDS_ar)
        {
            if (ID == ID_read)
                {
                    return false;
                }
        }

    return true;
}


