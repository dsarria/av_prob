field = importdata('GRRR_FIELD_1.txt');
energy = importdata('GRRR_ENERGY_1.txt');
prob = importdata('GRRR_PROB_1.txt');


% The matrix data needs to be field | energy| prob
% Alejandro made the data as (in python):

%    for i, ifield in enumerate(field):
%        for j, jeng in enumerate(eng):
%            fname = 'out/avalanche_%.4d_%.4d.dat' % (i, j)
%            try:
%                nparts = loadtxt(fname)
%                prob[i, j] = sum(nparts >= 10) / len(nparts)
%            except (TypeError, FileNotFoundError):
%                pass
%
%    savez(args.output, energy=eng, field=field, prob=prob)


% Meaning; for each field, it looped over all values of energy and calculated the prob.
%The original file has 200 values of field, 50 values of initial energy and 200x50 values of probability.

fileID = fopen('GRRR_data_1.txt','w');


size_f = size(field);
size_e = size(energy);
size_p = size(prob);



for i = 1:size_f(1)
	for j = 1:size_e(1)
		fprintf(fileID,'%1.5e \t %1.5e \t %1.5e  \n',field(i),energy(j), prob(i,j));
	end
end

fclose(fileID);

final_data = importdata('GRRR_data_1.txt');

size(final_data)



