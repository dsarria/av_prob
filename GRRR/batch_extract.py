from string import Template
from numpy import *
import scipy.constants as co
from matplotlib import pyplot as plt

def main():
    import argparse
    
    parser = argparse.ArgumentParser()

    parser.add_argument("output",
                        help="Output file", 
                        action='store')
    args = parser.parse_args()


    eng = logspace(1, 3, 50)
    field = logspace(0, log10(30), 200)

    prob = zeros((len(field), len(eng)))
    
    for i, ifield in enumerate(field):
        for j, jeng in enumerate(eng):
            fname = 'out/avalanche_%.4d_%.4d.dat' % (i, j)
            try:
                nparts = loadtxt(fname)
                prob[i, j] = sum(nparts >= 10) / len(nparts)
            except (TypeError, FileNotFoundError):
                pass

    savez(args.output, energy=eng, field=field, prob=prob)
    

                
if __name__ == '__main__':
    main()
