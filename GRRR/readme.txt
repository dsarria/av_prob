This directory contains data of avalanche probabilities calculated with grrr.

The procedure to obtain this data is as follows:

1. The python script batch.py generates reads the template
   avalanche_template.yaml and replaces the placeholders there to generate
   one input file per configuration (a pair of electric field-primary energy).

2. The bash script runbatch.sh runs grrr for each input file.  The option
   -N XXXX tells grrr to run XXXX instances of the simulation (here XXXX is 100)
   The maximum number of particles in each simulation is written in a line
   to an output file (the simulation stops with 10 particles).

3. The python script batch_extract.py reads these output files and counts
   the number of instances where the maximum of particles is 10 for each
   parameter pair.  With this, it calculates an avalanche probability that
   is written in an .npz file (compressed numpy format).

4. You can use batch_colormap.py to plot the results of the previous step.

