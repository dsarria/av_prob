#include "BaseRunAction.hh"

#include "G4Run.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "BaseAnalysisManager.hh"
#include "G4UnitsTable.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4ParticleTable.hh"
#include "G4EmCalculator.hh"

#include <time.h>
#include <sys/time.h>

#include <numeric>
#include <thread>

BaseRunAction::BaseRunAction()
{

}


BaseRunAction::~BaseRunAction()
{}


void BaseRunAction::BeginOfRunAction(const G4Run *)
{
    GLOB::COUNT_AVALAN = 0;
    GLOB::NB_LAUNCHED = 0;
}


void BaseRunAction::EndOfRunAction(const G4Run *)
{
    analysis->write_output_file();
}








