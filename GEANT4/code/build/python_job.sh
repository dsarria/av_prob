#!/bin/bash
#PBS -N python_geant4
#PBS -o /home/dsarria/geant4_projects/HEAPS/av_prob/GEANT4/code/build/out_file
#PBS -e /home/dsarria/geant4_projects/HEAPS/av_prob/GEANT4/code/build/err_file
#PBS -q furious
#PBS -M dsarria@apc.in2p3.fr
#PBS -l nodes=2:ppn=20,mem=40gb,walltime=1200:00:00

export SCRATCH="/scratch/$USER.$PBS_JOBID" 
export PATH=/usr/local/openmpi/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/openmpi/lib/:/usr/local/openmpi/lib/openmpi/:$LD_LIBRARY_PATH


cd /home/dsarria/geant4_projects/HEAPS/av_prob/GEANT4/code/build
mpirun -np 40 python g4_script_auto_mpi.py
