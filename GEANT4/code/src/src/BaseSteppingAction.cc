//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

#include "BaseSteppingAction.hh"
#include "BaseRunAction.hh"
#include "BaseDetectorConstruction.hh"

#include "G4SteppingManager.hh"
#include "G4VTouchable.hh"
#include "G4VPhysicalVolume.hh"

#include "BaseAnalysisManager.hh"
#include "G4SystemOfUnits.hh"
#include <iomanip>
#include <iostream>
#include <fstream>


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseSteppingAction::BaseSteppingAction(BaseDetectorConstruction *det)
    : Detector(det)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

BaseSteppingAction::~BaseSteppingAction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

void BaseSteppingAction::UserSteppingAction(const G4Step *aStep)
{

    GLOB::SAVED_TIME = aStep->GetTrack()->GetGlobalTime();

    if (GLOB::KILL_ALL_FLAG)
        {
            aStep->GetTrack()->SetTrackStatus(fStopAndKill);
            return;
        }

    G4int ID = aStep->GetTrack()->GetTrackID();
    G4double ener = aStep->GetTrack()->GetKineticEnergy();
    G4double timme = aStep->GetTrack()->GetGlobalTime();

    G4int PDG_enc = aStep->GetTrack()->GetParticleDefinition()->GetPDGEncoding();

    if (PDG_enc != 11) return;

    // if energy > 1 MeV and if the ID is not already saved
    if ((ener > 1.*MeV) && ID_not_already_saved(ID, GLOB::LIST_1MEV_IDS))
        {
            GLOB::LIST_1MEV_IDS.push_back(ID); // save the ID
            GLOB::COUNT_1MEV_ELEC++; // increment counter

            if (GLOB::COUNT_1MEV_ELEC >= GLOB::NB_1MEV_ELEC_REQ)
                {
                    GLOB::KILL_ALL_FLAG = true;
                    return;
                }
        }

    if (timme > GLOB::TIME_LIMIT)
        {
            aStep->GetTrack()->SetTrackStatus(fSuspend);
        }

    GLOB::SAVED_TIME = aStep->GetTrack()->GetGlobalTime();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....

bool BaseSteppingAction::ID_not_already_saved(const G4int ID, const std::vector<G4int> LIST_1MEV_IDS_ar)
{
    for (auto ID_read : LIST_1MEV_IDS_ar)
        {
            if (ID == ID_read)
                {
                    return false;
                }
        }

    return true;
}
