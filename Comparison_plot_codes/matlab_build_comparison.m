clear all
close all
clc

files={'./data/fused.txt','./data/GRRR_data_1.txt'};

physics_list_names{1}='O1';
physics_list_names{2}='O4';

%vector for lines in contour plot
v = [10,50];




fn = files{1};

for jj=1:2

LIST_selected=physics_list_names{jj};
DIRECTION_selected='+1';

% IMPORTING
file = importdata(fn);
DIRECTION = file.textdata(:,2);
LIST = file.textdata(:,3); %('O1' or 'O4')


SEED = file.textdata(:,1);
E = file.data(:,2);
ENERGY = file.data(:,3)*1000; % to keV
N_AV = file.data(:,1);
PROB = file.data(:,4);



%% filtering physics list
SEED=SEED(strcmp(LIST,LIST_selected));
E=E(strcmp(LIST,LIST_selected));
ENERGY=ENERGY(strcmp(LIST,LIST_selected));
N_AV=N_AV(strcmp(LIST,LIST_selected));
PROB=PROB(strcmp(LIST,LIST_selected));
DIRECTION=DIRECTION(strcmp(LIST,LIST_selected));

%% filtering direction
SEED=SEED(strcmp(DIRECTION,DIRECTION_selected));
E=E(strcmp(DIRECTION,DIRECTION_selected));
ENERGY=ENERGY(strcmp(DIRECTION,DIRECTION_selected));
N_AV=N_AV(strcmp(DIRECTION,DIRECTION_selected));
PROB=PROB(strcmp(DIRECTION,DIRECTION_selected));



%% CALCULATING
Efield = unique(E);
Efield_grid=sort(Efield);

% adding 0 field (for cosmetics in the plot)
Efield_grid=[0;Efield_grid];

Ep = unique(ENERGY);
ENERGY_grid=sort(Ep);

n1 = length(Efield_grid);
n2 = length(ENERGY_grid);

prob_den{jj} = zeros(n1,n2);
[xx,yy] = meshgrid(Efield_grid,ENERGY_grid);



for i=1:n1
    for j=1:n2
        id = logical( (E==Efield_grid(i)).*(ENERGY==ENERGY_grid(j)) );
        
        if sum(id)>0
            
            stats_ini=N_AV(id);
            proba=sum(PROB(id).*stats_ini)/sum(stats_ini);
            
            similar_probas_lists{i,j}=PROB(id);
            
            prob_den{jj}(i,j)=proba;
            
        end
        
    end
end

% PLOTTING
figure(jj)
h{jj}=pcolor(yy,xx,prob_den{jj}');
set(h{jj}, 'EdgeColor', 'none');
colorbar
axis([0 1000 200000 3000000])
title(['Probability to get Relativistic Avalanche; ' LIST_selected])
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')
set(gca,'xscale','log')
end


%% relative difference O1 and O4 physics lists



rela_diff = abs(prob_den{2}-prob_den{1})./prob_den{2} .*100;
rela_diff(isnan(rela_diff))=0;
rela_diff(isinf(rela_diff))=0;

% PLOTTING
figure
h100=pcolor(yy,xx,rela_diff');
set(h100, 'EdgeColor', 'none');
colorbar

title(['Relative difference (%) between O1 and O4 physics lists'])
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')


figr = importdata(files{2});

Egr = figr(:,1);
ENERGY_gr = figr(:,2);
PROB_gr = figr(:,3);


Efieldgr = unique(Egr);
Ep_gr = unique(ENERGY_gr);
ENERGY_grid_gr=sort(Ep_gr);
%Efield_grid_gr = Efieldgr;
Efield_grid_gr=sort(Efieldgr);
% adding 0 field (for cosmetics in the plot)
%Efield_grid_gr=[0;Efield_grid_gr];
n1gr = length(Efield_grid_gr);
n2gr = length(ENERGY_grid_gr);


%n1gr = length(Egr);
%n2gr = length(ENERGY_gr);





Egr=reshape(Egr,[n2gr,n1gr]);
ENERGY_gr = reshape(ENERGY_gr,[n2gr,n1gr]);
PROB_gr = reshape(PROB_gr,[n2gr,n1gr]);

%kV/cm to V/m 
Egr = Egr*10^5;
PROB_gr = PROB_gr*100.0;
figure

h = pcolor(ENERGY_gr,Egr,PROB_gr);

set(h, 'EdgeColor', 'none');
colorbar
axis([10 1000 200000 3000000])
%axis([0 1000 200 30])
%set(gca,'xscale','log')
title('Probability to get Relativistic Avalanche GRRR;')
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')



figure
hold on
gr = contour(ENERGY_gr,Egr,PROB_gr,v,'LineColor','k','ShowText','on');
g4 = contour(yy,xx,prob_den{2}',v,'LineColor','r','ShowText','on');
g1 = contour(yy,xx,prob_den{1}',v,'LineColor','b','ShowText','on');

axis([10 1000 200000 3000000])
%axis([0 1000 200 30])
set(gca,'xscale','linear')
title('Probability to get Relativistic Avalanche')
xlabel('Primary energy (keV)')
ylabel('Electric field (V/m)')
set(gca,'xscale','log')
text(690, 2.6E6, 'GRRR');
text(640, 2.6E6, '-', 'Color', 'k','FontWeight','bold');
text(690, 2.5E6, 'GEO4');
text(640, 2.5E6, '-', 'Color', 'r','FontWeight','bold');
text(690, 2.4E6, 'GEO1');
text(640, 2.4E6, '-', 'Color', 'b','FontWeight','bold');


