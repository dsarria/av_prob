#!/usr/bin/env python2


from mpi4py import MPI
from subprocess import call
from functools import partial
from multiprocessing.dummy import Pool
import numpy as np
import random
import time
import sys
from datetime import datetime


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

random.seed(datetime.now())
rand_seed = random.randint(1,100000000)

# Initializations and preliminaries
comm = MPI.COMM_WORLD   # get MPI communicator object
size = comm.Get_size()       # total number of processes
rank = comm.Get_rank()       # rank of this process
status = MPI.Status()   # get MPI status object

print(rank)
print(size)

# define the commands to be run in parallel
    
nr_particles = 5
nr_1MEV_elec_required = 20
#list_primary_energy = np.logspace(np.log10(0.010),np.log10(1.),50)  # MeV
list_primary_energy = [0.3]
#list_physicsList= ['O1','O4','SS']
#list_physicsList= ['O4','O1']
list_physicsList= ['O4']
#list_physics= ['LIV','O4','O1','PEN']
list_Efields = np.linspace(2e5,15e5,10) # V/m
list_direction=["+1"]
#max_step_list = [0.00001,0.00003,0.0001,0.0003,0.001,0.003,0.01,0.03,0.1,0.3,1,3,10,30.,100., 300.,1000.] # mm
#max_step_list = [0.001,0.003,0.01,0.03,0.1,0.3,1,3,10,30.,100., 300.,1000.] # mm
#dROverR_list = [0.8] # dimensionless
#dROverR_list = [0.00005, 0.00001,0.000005, 0.000001] # dimensionless
dROverR_list = [0.0001]
#max_step_list = [10.0,5.0,1.0,0.1,0.01] # cm
max_step_list = [0.01] # cm

nb_runs = 2000

commands=[]

excecutable = './BaseAV'

for direction in list_direction:
    for primary_energy in list_primary_energy:
        for field in list_Efields:
            for dROverR in dROverR_list:
                for physicsList in list_physicsList:
                    for _ in range(nb_runs):
                        for max_step in max_step_list:
                          commands.append(excecutable+' '+str(physicsList)+' '+str(nr_particles)+' '+str(field)
                                          +' '+str(primary_energy)+' '+str(nr_1MEV_elec_required)+' '+str(rand_seed)+' '+str(direction)+' '+str(dROverR)+' '+str(max_step))
                          rand_seed=rand_seed+1


# Define MPI message tags
tags = enum('READY', 'DONE', 'EXIT', 'START')

if rank == 0:
    # Master process executes code below
    tasks = commands
    task_index = 0
    num_workers = size - 1
    closed_workers = 0
    print("Master starting with %d workers" % num_workers)
    while closed_workers < num_workers:
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        source = status.Get_source()
        tag = status.Get_tag()
        if tag == tags.READY:
            # Worker is ready, so send it a task
            if task_index < len(tasks):
                comm.send(tasks[task_index], dest=source, tag=tags.START)
                print("Sending task %d to worker %d" % (task_index, source))
                task_index += 1
            else:
                comm.send(None, dest=source, tag=tags.EXIT)
        elif tag == tags.DONE:
            results = data
            print("Got data from worker %d" % source)
        elif tag == tags.EXIT:
            print("Worker %d exited." % source)
            closed_workers += 1

    print("Master finishing")
else:
    # Worker processes execute code below
    name = MPI.Get_processor_name()
    print("I am a worker with rank %d on %s." % (rank, name))
    while True:
        comm.send(None, dest=0, tag=tags.READY)
        task = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        tag = status.Get_tag()
        
        if tag == tags.START:
            # Do the work here
            task2=[task]
            pool = Pool(1) # to be always set to 1 for this MPI case
            for i, returncode in enumerate(pool.imap(partial(call, shell=True), task2)):
                if returncode != 0:
                    print("%d command failed: %d" % (i, returncode))
            comm.send(returncode, dest=0, tag=tags.DONE)
        elif tag == tags.EXIT:
            break

comm.send(None, dest=0, tag=tags.EXIT)
