from string import Template
from numpy import *
import scipy.constants as co
from matplotlib import pyplot as plt

def main():
    import argparse
    
    parser = argparse.ArgumentParser()

    parser.add_argument("input",
                        help="Input file", 
                        action='store')
    args = parser.parse_args()


    data = load(args.input)
    eng = data['energy']
    field = data['field']
    prob = data['prob']
    
    plt.pcolor(eng, field, 100 * prob, vmin=0, vmax=100)
    cbar = plt.colorbar()
    plt.semilogx()
    plt.ylim([0, 30])
    plt.xlabel("Primary electron energy (keV)")
    plt.ylabel("Electric field (kV/cm)")
    cbar.set_label("Avalanche probability (%)")
    
    plt.show()
    

                
if __name__ == '__main__':
    main()
